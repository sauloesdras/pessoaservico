package com.desafio.soa.pessoaservico.exception;

import lombok.Getter;

@Getter
public class NegocioException extends RuntimeException {

    private final String mensagem;

    private NegocioException(String mensagem) {
        super(mensagem);
        this.mensagem = mensagem;
    }

    public static NegocioException create(String mensagem) {
        return new NegocioException(mensagem);
    }

    public static void throwError(String mensagem) {
        throw new NegocioException(mensagem);
    }

}
