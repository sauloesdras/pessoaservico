package com.desafio.soa.pessoaservico.exception.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NegocioExceptionVO {
    private String mensagem;
}
