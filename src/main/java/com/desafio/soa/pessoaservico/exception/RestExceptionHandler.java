package com.desafio.soa.pessoaservico.exception;

import com.desafio.soa.pessoaservico.exception.vo.NegocioExceptionVO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value= {NegocioException.class})
    protected ResponseEntity<Object> handleBusinessException(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new NegocioExceptionVO(ex.getMessage()),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value= {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConstraintViolation(RuntimeException ex, WebRequest request) {
        ConstraintViolationException cve = (ConstraintViolationException) ex;

        String sbErro = "Erro ao gravar dados de pessoa: ";
        sbErro = sbErro.concat(
                cve.getConstraintViolations().stream()
                        .map(ConstraintViolation::getMessage)
                        .collect(Collectors.joining("; "))
        );

        return handleExceptionInternal(ex, new NegocioExceptionVO(sbErro),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }


}
