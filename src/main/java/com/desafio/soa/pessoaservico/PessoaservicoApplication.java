package com.desafio.soa.pessoaservico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PessoaservicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PessoaservicoApplication.class, args);
	}

}
