package com.desafio.soa.pessoaservico.rest;

import com.desafio.soa.pessoaservico.service.PessoaService;
import com.desafio.soa.pessoaservico.service.dto.PessoaDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pessoa")
public class PessoaResource {

    private final PessoaService pessoaService;

    @PostMapping("/buscar")
    public ResponseEntity<Page<PessoaDTO>> buscar(@RequestBody PessoaDTO filtro, Pageable pageable) {
        Page<PessoaDTO> page = pessoaService.buscar(filtro, pageable);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PessoaDTO> salvar(@RequestBody PessoaDTO usuarioDto) {
        return new ResponseEntity<>(pessoaService.salvar(usuarioDto), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PessoaDTO> recuperar(@PathVariable Integer id) {
        return new ResponseEntity<>(pessoaService.recuperar(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> excluir(@PathVariable Integer id) {
        pessoaService.excluir(id);
        return ResponseEntity.ok().build();
    }

}
