package com.desafio.soa.pessoaservico.service.impl;

import com.desafio.soa.pessoaservico.domain.Pessoa;
import com.desafio.soa.pessoaservico.exception.NegocioException;
import com.desafio.soa.pessoaservico.repository.PessoaRepository;
import com.desafio.soa.pessoaservico.service.PessoaService;
import com.desafio.soa.pessoaservico.service.dto.PessoaDTO;
import com.desafio.soa.pessoaservico.service.mapper.PessoaMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Service
@Transactional
@RequiredArgsConstructor
public class PessoaServiceImpl implements PessoaService {

    private final PessoaRepository pessoaRepository;
    private final PessoaMapper pessoaMapper;

    private final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private final Validator validator = factory.getValidator();

    @Override
    public Page<PessoaDTO> buscar(PessoaDTO exemplo, Pageable pageable) {

        ExampleMatcher matchers = ExampleMatcher.matching()
                .withMatcher("nome", contains().ignoreCase())
                .withMatcher("cpf", contains().ignoreCase());

        Example<Pessoa> example = Example.of(pessoaMapper.toEntity(exemplo), matchers);
        return pessoaRepository.findAll(example, pageable).map(pessoaMapper::toDto);
    }

    @Override
    public PessoaDTO salvar(PessoaDTO pessoaDTO) {
        validarCadastro(pessoaDTO);
        Pessoa pessoa = pessoaRepository.save(pessoaMapper.toEntity(pessoaDTO));
        return pessoaMapper.toDto(pessoa);
    }

    @Override
    public PessoaDTO recuperar(Integer id) {
        Pessoa pessoa = findById(id);
        return pessoaMapper.toDto(pessoa);
    }

    @Override
    public void excluir(Integer id) {
        Pessoa pessoa = findById(id);
        pessoaRepository.delete(pessoa);
    }

    private void validarCadastro(PessoaDTO pessoaDTO) {
        Set<ConstraintViolation<PessoaDTO>> cvs = validator.validate(pessoaDTO);
        if(!cvs.isEmpty()){
            throw new ConstraintViolationException(cvs);
        }

        pessoaRepository.findByCpf(pessoaDTO.getCpf()).ifPresent( p -> {
                if(!p.getId().equals(pessoaDTO.getId())){
                    NegocioException.throwError("Erro ao gravar dados de pessoa: CPF duplicado");
                }
        });

    }

    private Pessoa findById(Integer id){
        return pessoaRepository.findById(id).orElseThrow(() ->
                NegocioException.create("Registro não encontrado"));
    }
}
