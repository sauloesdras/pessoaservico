package com.desafio.soa.pessoaservico.service.mapper;

import com.desafio.soa.pessoaservico.domain.Pessoa;
import com.desafio.soa.pessoaservico.service.dto.PessoaDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PessoaMapper {
    PessoaDTO toDto(Pessoa usuario);
    Pessoa toEntity(PessoaDTO usuarioDto);
}
