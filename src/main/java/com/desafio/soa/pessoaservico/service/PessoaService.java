package com.desafio.soa.pessoaservico.service;

import com.desafio.soa.pessoaservico.service.dto.PessoaDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PessoaService {
    Page<PessoaDTO> buscar(PessoaDTO filter, Pageable pageable);
    PessoaDTO salvar(PessoaDTO pessoaDTO);
    PessoaDTO recuperar(Integer id);
    void excluir(Integer id);
}
