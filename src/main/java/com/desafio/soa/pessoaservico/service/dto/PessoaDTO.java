package com.desafio.soa.pessoaservico.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class PessoaDTO implements Serializable {

    private Integer id;
    @NotNull(message = "Nome Obrigatório")
    private String nome;
    @NotNull(message = "CPF Obrigatório")
    @CPF(message = "CPF Inválido")
    private String cpf;
    @NotNull(message = "Data de Nascimento Obrigatória")
    private LocalDate dataNascimento;

}
