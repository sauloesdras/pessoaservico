package com.desafio.soa.pessoaservico.rest;

import com.desafio.soa.pessoaservico.PessoaservicoApplication;
import com.desafio.soa.pessoaservico.service.dto.PessoaDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.persistence.EntityManager;
import java.time.LocalDate;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = PessoaservicoApplication.class)
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class PessoaServicoRestTests {

    private static final ObjectMapper mapper = createObjectMapper();

    private static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    @Autowired
    private EntityManager em;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    @Transactional
    void deveSalvar() throws Exception {
        salvarPessoa(construirDTO());
        PessoaDTO pessoa = new PessoaDTO(null, "Nome Pessoa", "20858755726", LocalDate.now());
        salvarPessoa(pessoa);

        mockMvc.perform(post("/api/pessoa/buscar")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsBytes(new PessoaDTO())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.numberOfElements", is(2)));
    }

    @Test
    @Transactional
    void deveRejeitarCadastroCpfInvalido() throws Exception {
        PessoaDTO pessoa = new PessoaDTO(null, "Nome Pessoa", "7362342888488", LocalDate.now());
        mockMvc.perform(post("/api/pessoa")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsBytes(pessoa)))
                .andExpect(status().is(400));

    }

    @Test
    @Transactional
    void deveRejeitarCadastroCpfDuplicado() throws Exception {
        salvarPessoa(construirDTO());
        mockMvc.perform(post("/api/pessoa")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsBytes(construirDTO())))
                .andExpect(status().is(400));

    }

    @Test
    @Transactional
    void deveRecuperar() throws Exception {
        Integer id = salvarPessoa(construirDTO());
        mockMvc.perform(get("/api/pessoa/{id}", id)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cpf", is("73623428884")));
    }

    @Test
    @Transactional
    void deveAtualizar() throws Exception {
        PessoaDTO pessoa = construirDTO();
        Integer id = salvarPessoa(pessoa);
        pessoa.setId(id);
        pessoa.setNome("ATUALIZADO");
        salvarPessoa(pessoa);

        mockMvc.perform(get("/api/pessoa/{id}", id)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome", is("ATUALIZADO")));
    }

    @Test
    @Transactional
    void deveExcluir() throws Exception {
        Integer id = salvarPessoa(construirDTO());

        mockMvc.perform(delete("/api/pessoa/{id}", id)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/pessoa/{id}", id)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(400));
    }



    PessoaDTO construirDTO() {
        return new PessoaDTO(null, "Nome Pessoa", "73623428884", LocalDate.now());
    }

    Integer salvarPessoa(PessoaDTO pessoa) throws Exception{
        MvcResult result = mockMvc.perform(post("/api/pessoa")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsBytes(pessoa)))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        return JsonPath.parse(response).read("$.id");
    }

}
